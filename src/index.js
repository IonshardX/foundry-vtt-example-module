import { canvasReady, ready } from "./hooks";

Hooks.on("canvasReady", canvasReady);
Hooks.on("ready", ready);
