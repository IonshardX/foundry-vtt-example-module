# Foundry VTT - Example Module

## Setup

### Customization
Maybe eventually I will convert this into a generator package. Until then you will want to customize this example module for your own purposes.

#### Module Customization
Update the `package.json` file. This is where your module's `module.json` is generated from.

Most notably update the following fields:

- name
- title
- version
- description
- author

Furthermore you will want to change the module name in the `README.md` top level header.

#### License Customization

Finally update the License by changing the name in the `README.md` License header and by changing the name in the `LICENSE.md` file.

### NPM

Run the following command, in order to retrieve the dependencies needed to build the module.

```bash
npm install
```

## Usage

To build the module you can run:

```bash
npm run build
```

Which will bundle up the Javascript and create the module.json file inside of the `dist/` folder.

You can copy this folder into the modules directory for Foundry VTT to install it.

### Linting

Liniting is configured in this application using the AirBnB config.

```bash
npm run lint
```

or you can run

```bash
npm run lint:fix
```

to attempt to fix any issue found.

Furthermore this repo uses [Husky](https://github.com/typicode/husky#readme) to hook the linting up to git hooks to ensure only clean code is commited to the repo.

### Git Flow

I highly recommend using the [git-flow](https://jeffkreeftmeijer.com/git-flow/) branching strategy.

### GitLab CI/CD

This repo includes a `.gitlab-ci.yml` file which will facilitate GitLab in building your module and using the artifacts to handle the module ZIP files.

## License
<a rel="license" href="https://spdx.org/licenses/MIT.html"><img alt="MIT License" style="border-width:0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/License_icon-mit-88x31-2.svg/88px-License_icon-mit-88x31-2.svg.png" /></a> Example Module - a module for Foundry VTT - by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/Ionshard/foundry-vtt-anvil-menu" property="cc:attributionName" rel="cc:attributionURL">Victor Ling</a> is licensed under an <a rel="license" href="https://spdx.org/licenses/MIT.html"> MIT License</a>.

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for Module Development v0.3.8](http://foundryvtt.com/pages/license.html).


